package com.huang.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
//写这个注解，后续不用在mapper上面标@Mapper
@MapperScan("com.huang.cloud.mapper") //import tk.mybatis.spring.annotation.MapperScan;
//启用服务发现
@EnableDiscoveryClient
@RefreshScope//动态刷新配置
public class Main8001 {
    public static void main(String[] args) {
        SpringApplication.run(Main8001.class,args);
    }
}
